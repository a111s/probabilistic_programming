from matplotlib import pyplot as plt
from mpl_toolkits import mplot3d
from scipy.stats import beta
from functools import reduce
import numpy as np

#############
### prior ###
#############
# def prior(ω, θ):
#     κ = 12
    
#     a25 = .25*(κ-2)+1
#     b25 = (1-.25)*(κ-2)+1
#     n25 = reduce(lambda x,y: x+y, [beta(a25,b25).pdf(x)*beta(1,1).pdf(x) for x in np.arange(0,1,.01)])

#     a75 = .75*(κ-2)+1
#     b75 = (1-.75)*(κ-2)+1
#     n75 = reduce(lambda x,y: x+y, [beta(a75,b75).pdf(x)*beta(1,1).pdf(x) for x in np.arange(0,1,.01)])

#     nor = n25+n75

#     def g(i,j):
#         result = 0
#         if (i==.25) or (i==.75):
#             a = i*(κ-2)+1
#             b = (1-i)*(κ-2)+1
#             result = (beta(a,b).pdf(j)*beta(1,1).pdf(i)) / nor
#         return result
    
#     return np.array([[ g(ω[i][j],θ[i][j]) for j in range(0,len(ω))] for i in range(0,len(θ))])

# x = np.arange(0, 1, .01)
# y = np.arange(0, 1, .01)

# X, Y = np.meshgrid(x,y)
# priorZ = prior(X, Y)

# fig = plt.figure(figsize=(5,5))
# ax = plt.axes(projection='3d')
# #ax.plot_wireframe(X, Y, likelihoodZ, rstride=4, cstride=1)
# ax.contour3D(X, Y, priorZ, 70, cmap='viridis')
# ax.set_xlabel('ω')
# ax.set_ylabel('θ')
# ax.set_zlabel('prior')
# ax.invert_xaxis()  # To show it like in the book


# ##################
# ### Likelihood ###
# ##################
# def likelihood(ω, θ):
#     z=6
#     N=9
#     bernoulli = lambda theta,z,N: theta**(z) * (1-theta)**(N-z)    
#     return np.array([[ bernoulli(θ[i][j],z,N) for j in range(0,len(ω))] for i in range(0,len(θ))])

# x = np.arange(0, 1, .01)
# y = np.arange(0, 1, .01)

# X, Y = np.meshgrid(x,y)
# likelihoodZ = likelihood(X, Y)

# fig = plt.figure(figsize=(5,5))
# ax = plt.axes(projection='3d')
# #ax.plot_wireframe(X, Y, likelihoodZ, rstride=4, cstride=1)
# ax.contour3D(X, Y, likelihoodZ, 70, cmap='viridis')
# ax.set_xlabel('ω')
# ax.set_ylabel('θ')
# ax.set_zlabel('likelihood')
# ax.invert_xaxis()  # To show it like in the book


# ###############
# ## posterior ##
# ###############
# x = np.arange(0, 1, .01)
# y = np.arange(0, 1, .01)

# X, Y = np.meshgrid(x,y)
# posteriorZ = priorZ*likelihoodZ

# fig = plt.figure(figsize=(5,5))
# ax = plt.axes(projection='3d')
# #ax.plot_wireframe(X, Y, posteriorZ, rstride=4, cstride=1)
# ax.contour3D(X, Y, posteriorZ, 70, cmap='viridis')
# ax.set_xlabel('ω')
# ax.set_ylabel('θ')
# ax.set_zlabel('posterior')
# ax.invert_xaxis()  # To show it like in the book

def prior(ω, θ):
    κ = 12
    
    a25 = .25*(κ-2)+1
    b25 = (1-.25)*(κ-2)+1
    n25 = reduce(lambda x,y: x+y, [beta(a25,b25).pdf(x)*beta(1,1).pdf(x) for x in np.arange(0,1,.01)])

    a75 = .75*(κ-2)+1
    b75 = (1-.75)*(κ-2)+1
    n75 = reduce(lambda x,y: x+y, [beta(a75,b75).pdf(x)*beta(1,1).pdf(x) for x in np.arange(0,1,.01)])

    nor = n25+n75
    
    def g(i,j):
        result = 0
        if (i==.25) or (i==.75):
            a = i*(κ-2)+1
            b = (1-i)*(κ-2)+1
            result = (beta(a,b).pdf(j)*beta(1,1).pdf(i)) / nor
        return result
    
    return np.array([[ g(ω[i][j],θ[i][j]) for j in range(0,len(ω))] for i in range(0,len(θ))])

def likelihood(ω, θ):
    z=6
    N=9
    bernoulli = lambda theta,z,N: theta**(z) * (1-theta)**(N-z)    
    return np.array([[ bernoulli(θ[i][j],z,N) for j in range(0,len(ω))] for i in range(0,len(θ))])


x = np.arange(0, 1, .01)
y = np.arange(0, 1, .01)

X, Y = np.meshgrid(x,y)
priorZ = prior(X,Y)
likelihoodZ = likelihood(X,Y)
posteriorZ = likelihoodZ*priorZ


fig = plt.figure()

ax = fig.add_subplot(1, 3, 1, projection='3d')
#ax.plot_wireframe(X, Y, priorZ, rstride=4, cstride=1)
ax.contour3D(X, Y, priorZ, 70, cmap='viridis')
ax.set_xlabel('ω')
ax.set_ylabel('θ')
ax.set_zlabel('prior')
ax.invert_xaxis()  # To show it like in the book
ax.set_title('Prior')

ax1 = fig.add_subplot(1, 3, 2, projection='3d')
#ax1.plot_wireframe(X, Y, likelihoodZ, rstride=4, cstride=1)
ax1.contour3D(X, Y, likelihoodZ, 70, cmap='viridis')
ax1.set_xlabel('ω')
ax1.set_ylabel('θ')
ax1.set_zlabel('likelihood')
ax1.invert_xaxis()  # To show it like in the book
ax1.set_title('Likelihood')

ax2 = fig.add_subplot(1, 3, 3, projection='3d')
#ax2.plot_wireframe(X, Y, posteriorZ, rstride=4, cstride=1)
ax2.contour3D(X, Y, posteriorZ, 70, cmap='viridis')
ax2.set_xlabel('ω')
ax2.set_ylabel('θ')
ax2.set_zlabel('posterior')
ax2.invert_xaxis()  # To show it like in the book
ax2.set_title('Posterior')


plt.show()
