{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Chapter 2, Example analysis of Height/Weight correlation"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Intro"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "* To use this file start jupyter-lab in the same directory, open the file from the UI, and execute step-by-step (Shift-Enter).\n",
    "\n",
    "* The file follows the weight-height example from Chapter 2 in (Kruschke,2015)\n",
    "\n",
    "* Ignore a ```theano.tensor.blas``` warning below. It seems to be a rather unimportant configuration problem showing up only on some computers (probably a sign of slightly broken setup of Python3 et al)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import pymc3 as pm\n",
    "from theano import shared\n",
    "import numpy as np\n",
    "import pandas as pd\n",
    "import arviz as az\n",
    "from matplotlib import pyplot as plt"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Section 2.3 Steps of Bayesian data analysis\n",
    "We start the example from Section 2.3 in the puppies book here. \n",
    "\n",
    "* \"Suppose we have been able to collect heights and weights from ```size``` mature adults sampled at random from a population of interest.\"\n",
    "\n",
    "* Heights are measured on the continuous scale of inches\n",
    "\n",
    "* Weights are measured on the continuous scale of pounds\n",
    "\n",
    "* We wish to predict weight from height\n",
    "\n",
    "* We begin by declaring a sample `size` then loading the data to be analyzed, fixing the types, and showing the prefix of the data so that you know what is in the file.  Feel free to inspect the CSV file in a text editor.  Incidentally, you can also open it in Jupyter Lab, which displays CSV files in a tabular format.\n",
    "\n",
    "* The data comes from NHIS, 2007, https://www.cdc.gov/nchs/nhis/index.htm?CDC_AA_refVal=https%3A%2F%2Fwww.cdc.gov%2Fnchs%2Fnhis.htm (reduced to the right columns), so we use a different data set than the book."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "size = 500\n",
    "data = pd.read_csv('nhis-modified.csv').head(size) \n",
    "data.head()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.figure(figsize=(7, 7))\n",
    "plt.plot(data['height'], data['weight'], 'x', label='data')\n",
    "plt.title('A scatter plot of the data (cf. book Fig. 2.5)')\n",
    "plt.legend(loc=0)\n",
    "plt.axis([55, 85, 75, 175])\n",
    "plt.xlabel('height in inches')\n",
    "plt.ylabel('weight in pounds')\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "matplotlib.pyplot.figure(num=None, figsize=None, dpi=None, facecolor=None, edgecolor=None, frameon=True, FigureClass=<class 'matplotlib.figure.Figure'>, clear=False, **kwargs)\n",
    "figsize : (float, float), optional, default: None\n",
    "width, height in inches. If not provided, defaults to rcParams[\"figure.figsize\"] = [6.4, 4.8] = [6.4, 4.8]."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "* Our goal is to predict weight by height (it is plausible that these are correlated).  So we identified the variables, and roles between them.\n",
    "\n",
    "* We need to build a predictive model. We will do it in the pymc3 framework, which works with vectors of numbers. \n",
    "\n",
    "* We load the vector of heights into `x`, and the vector of weights into `y`.  This way we are also closer to the book presentation.\n",
    "\n",
    "* We are using the `with` statement below (see https://docs.python.org/3/reference/compound_stmts.html#the-with-statement). Quoting:  *The with statement is used to wrap the execution of a block with methods defined by a context manager*.  In some weird sense it is \"monadic Python\", whatever that means (https://stackoverflow.com/questions/7131027/is-pythons-with-monadic). Basically it sets a state object that is going to hold the framework statement. A more palatable introduction to `with`: https://preshing.com/20110920/the-python-with-statement-by-example/ . In pymc3 the `with` construct allows to work with more than one model at a time."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "with pm.Model() as model:\n",
    "    x    = data['height'].to_numpy()\n",
    "    x_shared = shared (x) #ignore for now\n",
    "    y    = data['weight'].to_numpy()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "* We are assuming a **linear model**: $y = \\beta_1 x + \\beta_0$. We will be solving a **linear regression task**, as opposed to a classification/decision task. The linear equation is not programmed above. We do it a little below, when we have the full model.\n",
    "\n",
    "* We would like to optimize the values of parameters $\\beta_1$ and $\\beta_2$ to **fit best our training data set**.\n",
    "\n",
    "* $\\beta_1$ is the **slope** parameter (says how many pounds the predicted weight increases, when height goes up by 1 inch)\n",
    "\n",
    "* $\\beta_2$ is the **shift** parameter (aka **intercept**, so the expected mean value of height when all weights are zero).\n",
    "\n",
    "* Since we do not know anything upfront about this parameters, let's assume a **uniform prior** for them. \n",
    "\n",
    "* A uniform probability (here) is a constant function assigning the same density to all values.\n",
    "\n",
    "* We choose a large interval as domain, and allow both positives and negative slops as to have minimal prior influence on the regression."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "with model:        # This form of with allows us to work in the context of the previously created and named model object\n",
    "    β0 = pm.Uniform('β0', -200, 200)\n",
    "    β1 = pm.Uniform('β1', -200, 200)\n",
    "    \n",
    "    # lDiscreteUniform = pm.DiscreteUniform('l', 20 , 200)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "* Weight is a **physical parameter**, so it will not be distributed deterministically with the above linear model, but in reality it will have some **noise**.  \n",
    "\n",
    "* We choose the linear model to define the **expected** weight, and we allow some deviations from that.  Think about this again: this means that $y$ is not the predicted weight, but the predicted expected weight. A slight philosophical difference that manifests below. \n",
    "\n",
    "* The deviations (noise) are modelled using a standard Gaussian distribution (a **Normal distribution**)\n",
    "\n",
    "* The **standard deviation** will be $\\sigma$ with a uniform prior (we do not know what to expect).  The $\\sigma$ parameter will tell us how much noise we expect in the predictions, or how much the actual weight vary around the expected weight.\n",
    "\n",
    "* Note that the **mean** parameter $\\mu$ **is not really a parameter**. This is what we are predicting, so $\\mu=y=\\beta_1x + \\beta_0$ so it deterministically depends on $\\beta_1$ and $\\beta_0$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "with model:\n",
    "    σ  = pm.Uniform('σ', 0, 100)\n",
    "    likelihood = pm.Normal('obs', mu=β1*x_shared + β0, sigma=σ, observed=y)  # here the linear regression model actually appears with all the parameters"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "* The argument ```observed=y``` above means that we have a sample for this model. This refers to weights, so ```data['weight']```\n",
    "\n",
    "* Above, ```sigma``` and ```mu``` are just named function parameters in Python.  ```y``` and ```x``` are our arrays with data.  $\\sigma$, $\\beta_0$, $\\beta_1$ are parameters.\n",
    "\n",
    "* The name likelihood is almost like probability, except that probability is computed for fixed parameters for values of the domain. For likelihood, we fix the domain values, and will vary paramters to find the optimal ones.\n",
    "\n",
    "* Finally, we use a sampling algorithm to create a sample from our model (and infer the posterior)\n",
    "\n",
    "* This step can take some time (30s on my machine).  By default `sample` uses up to 4 cores on your machine.  If you want to set more, add the `cores=n` as a named argument."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "with model:\n",
    "    trace = pm.sample(1000,  tune=3000)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    " tune : int\n",
    "        Number of iterations to tune, defaults to 500. Samplers adjust the step sizes, scalings or\n",
    "        similar during tuning. Tuning samples will be drawn in addition to the number specified in\n",
    "        the ``draws`` argument, and will be discarded unless ``discard_tuned_samples`` is set to\n",
    "        False."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "* Now for the plotting.\n",
    "* The following plot shows both the data points (like above) and a range of plausible regression lines.  \n",
    "* This is mostly for sanity check, so we can see that they make sense.\n",
    "* Another important learning point: it reminds that with Bayesian analysis we rarely get single values. We mostly get ranges of values, or more precisely **distributions**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "with model:\n",
    "    plt.figure(figsize=(7, 7))\n",
    "    plt.axis([60, 75, 75, 175])\n",
    "    plt.plot(x, y, 'x', label='data')\n",
    "    pm.plot_posterior_predictive_glm(trace, samples=100,\n",
    "                                     eval=x,\n",
    "                                     lm=lambda x, sample: sample['β1'] * x + sample['β0'],\n",
    "                                     label='posterior predictive regression', c='C2')\n",
    "    plt.title('Posterior predictive regression lines')\n",
    "\n",
    "    plt.legend(loc=0)\n",
    "    plt.xlabel('height [pounds]')\n",
    "    plt.ylabel('weight [inches]')\n",
    "    plt.show()    "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "* But what if we have to build a linear predictor? We have to pick up a concrete value. Mean (the expected value) or mode (the most probable value) are typical choices.  \n",
    "* For expectations: The expected value of a random vector is a vector whose elements are the expected values of the individual random variables that are the elements of the random vector."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "with model:\n",
    "    print (f\"The predictor using expected values y = {trace['β1'].mean():.2f} * x + {trace['β0'].mean():.2f}   ± {trace['σ'].mean():.2f}\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "* A more Bayesian way would be to selected maximum probability values for parameters from the posterior (see the graph below)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "* Let us now try to understand the density of plausible values for $\\beta_1$.\n",
    "* The inference has obtained a joint probability distribution for values of $\\beta_1$, $\\beta_0$, and $\\sigma$ (think: they come in triples!)\n",
    "* The following graph (cf. Fig 2.5 in the puppies book) shows the marginal distribution for $\\beta_1$ (when we ignore the other components -- we will explain what marginal means later in the course)\n",
    "* High densities values (around the mode) are most credible.\n",
    "* We got a slightly different value than Kruschke (but we also used a different training set)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "with model:\n",
    "    pm.plot_posterior(trace, \n",
    "                      var_names=['β1'], \n",
    "                      point_estimate='mode', \n",
    "                      credible_interval=0.95, \n",
    "                      kind=\"hist\", \n",
    "                      ref_val=0, \n",
    "                      bins=20, \n",
    "                      rwidth=0.85)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "* HDP = Highest posterior density interval, also: https://en.wikipedia.org/wiki/Credible_interval . This is the same thing call HDI (highest density interval) in by Kruschke\n",
    "* It means that values of $\\beta_1$ (slope) outside $[2.6,3.5]$ are very unlikely given our training data set. We can rule them out probabilistically.\n",
    "* The orange line, marks the slope.  This is just an example to relate to hypothesis testing in classical statistics.  If $\\beta_1 = 0$ was our hypothesis, we can safely reject it based on this analysis. \n",
    "* The orange label over the curve is related. It indicates how big HDP/HDI would have to be to accommodate the hypothesis value. Here it means that zero is basically an impossible value for the parameter, according to sampling (a very strong result, for our silly hypothesis).\n",
    "* We will now do the final step - checking the validity of the model (the posterior predictive check)\n",
    "* This also shows how to create a probabilistic predictor (so better than using mean, and better than using mode).  Instead of predicting with a concrete $(\\beta_1, \\beta_0)$ pair, we are predicting with a **distribution** of such pairs, obtaining a range of predictions, or a credible interval of predictions (we can also take a mean of them)\n",
    "* Feel free to ignore the code below and focus on understanding the resulting graph.  "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "xpoints = np.linspace(50,80,20)\n",
    "with model:\n",
    "    x_shared.set_value(xpoints)\n",
    "    ppc = pm.sample_posterior_predictive(trace, \n",
    "                                         samples=500, \n",
    "                                         model=model, \n",
    "                                         var_names=['obs'])\n",
    "    hdp_obs = pm.stats.hpd(ppc['obs'])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "\n",
    "ymeans = [np.mean(np.array([x[i] for x in ppc['obs']])) \n",
    "             for i in np.arange(0,20)]\n",
    "ymaxs = [y[1] for y in hdp_obs]\n",
    "ymins = [y[0] for y in hdp_obs]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.figure(figsize=(7, 7))\n",
    "plt.plot(data['height'], data['weight'], 'xk', label='data')\n",
    "plt.title('A scatter plot of the data (cf. book Fig. 2.5)')\n",
    "\n",
    "plt.axis([55, 82, 75, 175])\n",
    "plt.xlabel('height in inches')\n",
    "plt.ylabel('weight in pounds')\n",
    "   \n",
    "plt.vlines (xpoints, ymaxs, ymins, '#87B5D3')\n",
    "plt.plot(xpoints, ymeans, 'o', color='#87B5D3',label='pred. means') \n",
    "\n",
    "plt.legend(loc=0)\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "* For 20 points (fixed weight points), we predict height using a distribution of parameters from the posterior (500 triples per x entry)\n",
    "* We draw the credible interval (HDP/HDI) for each of them, and mark the mean inthe middle.\n",
    "* It is quite clear that the approximation of the data is decent (the actual data set drawn with black crosses is plausible for this model)."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.1"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
